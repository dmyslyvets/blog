﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BlogNLayer.Entities;
using BlogNLayer.ViewModels.CommentViewModels;

namespace BlogNLayer.ViewModels.ArticleViewModels
{
    public class ArticleViewModel
    {
        public Guid Id { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
        public string Author { get; set; }
        public DateTime DateTime { get; set; }
        public List<CommentViewModel> Comment { get; set; }
        public List<Category> Category { get; set; }  
    }
}
