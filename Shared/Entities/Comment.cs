﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity;
using System.ComponentModel.DataAnnotations;
using Dapper.Contrib.Extensions; 
using System.ComponentModel.DataAnnotations.Schema;

namespace BlogNLayer.Entities
{
    public class Comment : BaseEntity
    {
        public string NameAuthor { get; set; }
        public string Commentary { get; set; }
        public DateTime DateTime { get; set; }

        
        [ForeignKey("ArticleId")]
        [Write(false)]
        public Article Article { get; set; }

        public string ArticleId { get; set; }
    }
}
