﻿using Dapper.Contrib.Extensions;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BlogNLayer.Entities
{
    [System.ComponentModel.DataAnnotations.Schema.Table("CategoryInArticles")]
    public class CategoryInArticle : BaseEntity
    {
        public Guid ArticleId { get; set; }
        public Guid CategoryId { get; set; }

        [ForeignKey("ArticleId")]
        [Write(false)]
        public Article Article { get; set; }

        [ForeignKey("CategoryId")]
        [Write(false)]
        public Category Category { get; set; }
    }
}
