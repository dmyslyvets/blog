﻿using System;
using System.Collections.Generic;
using AutoMapper;
using BlogNLayer.Entities;
using BlogNLayer.Repositories;
using BlogNLayer.Service.Mappers;
using BlogNLayer.ViewModels.CategoryViewModels;

namespace BlogNLayer.Service
{
    public class CategoryService
    {
        private CategoryRepositories _categoryRepositories;
        private CategoryInArtycleRepositories _categoryInArtycleRepositories;
       
        public CategoryService(string connectionString)
        {
            _categoryRepositories = new CategoryRepositories( connectionString);
            _categoryInArtycleRepositories = new CategoryInArtycleRepositories(connectionString);
        }

        public List<CategoryViewModel> GetAllCategoryViews()
        {
            List<CategoryViewModel> categoriesView;
            List<Category> categories = _categoryRepositories.GetAll();
            categoriesView = Mapper.Map<List<Category>, List<CategoryViewModel>>(categories);
            return categoriesView;
        }

        public Category GetCategory(string id)
        {
            return _categoryRepositories.Get(id);
        }

        public void Create(CategoryPostViewModel CategoryViewModel)
        {
            Category category = Mapper.Map<CategoryPostViewModel, Category>(CategoryViewModel);
            if (category != null)
            {
                _categoryRepositories.Create(category);
            }
        }

        public void Delete(string id)
        {
            _categoryInArtycleRepositories.DeleteCategory(id);
            _categoryRepositories.Delete(id);
        }
    }
}
