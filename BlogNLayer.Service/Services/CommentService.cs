﻿using System;
using System.Collections.Generic;
using System.Linq;
using AutoMapper;
using BlogNLayer.Entities;
using BlogNLayer.Repositories;
using BlogNLayer.Service.Mappers;
using BlogNLayer.ViewModels.CommentViewModels;

namespace BlogNLayer.Service
{
    public class CommentService
    {
        private CommentRepositories _commentRepository;
        
        public CommentService(string connectionString)
        {           
            _commentRepository = new CommentRepositories(connectionString);
        }

        public List<CommentViewModel> GetAllCommentsForArticle(string idArticle)
        {
            List<CommentViewModel> commentsView = null;
            List<Comment> Comments = _commentRepository.GetAllForArticle(idArticle);
            commentsView = Mapper.Map<List<Comment>, List<CommentViewModel>>(Comments);
            return commentsView;
        }

        public void Create(CommentPostViewModel commentView)
        {
            Comment comment = Mapper.Map<CommentPostViewModel, Comment>(commentView);
            comment.DateTime = DateTime.Now;
            _commentRepository.Create(comment);
        }

        public void Delete(string id)
        {
            _commentRepository.Delete(id);
        }
    }
}
