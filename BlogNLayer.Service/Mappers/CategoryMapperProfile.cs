﻿using AutoMapper;
using BlogNLayer.Entities;
using BlogNLayer.ViewModels;
using BlogNLayer.ViewModels.ArticleViewModels;
using BlogNLayer.ViewModels.CategoryViewModels;
using BlogNLayer.ViewModels.CommentViewModels;

namespace BlogNLayer.Service.Mappers
{
    public class CategoryMapperProfile: Profile
    {
        public CategoryMapperProfile()
        {
           CreateMap<Category, CategoryViewModel>();
           CreateMap<CategoryPostViewModel, Category>();
        }
    }
}
