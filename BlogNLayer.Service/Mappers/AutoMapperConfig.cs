﻿using AutoMapper;

namespace BlogNLayer.Service.Mappers
{
    public static class AutoMapperConfig
    {
        public static void Initialize()
        {
            Mapper.Initialize(config =>
            {
                config.AddProfile(new ArticleMapperProfile());
                config.AddProfile(new CategoryMapperProfile());
                config.AddProfile(new CommentMapperProfile());
            });
        }
    }
}
