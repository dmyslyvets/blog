﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity;
using BlogNLayer.Entities;
using System.Data;
using System.Data.SqlClient;
using Dapper;
using Dapper.Contrib.Extensions;

namespace BlogNLayer.Repositories
{
    public class CategoryInArtycleRepositories
    {
        private string _connectionString;

        public CategoryInArtycleRepositories(string connectionString)
        {
            _connectionString = connectionString;
        }

        public IEnumerable<CategoryInArticle> GetAll()
        {
            IEnumerable<CategoryInArticle> articlesInCategories;
          
            using (IDbConnection db = new SqlConnection(_connectionString))
            {
                articlesInCategories = db.GetAll<CategoryInArticle>().ToList();
            }
            return articlesInCategories;
        }

        public CategoryInArticle Get(string id)
        {
            CategoryInArticle articlesInCategory = new CategoryInArticle();
          
            using (IDbConnection db = new SqlConnection(_connectionString))
            {
                articlesInCategory = db.Get<CategoryInArticle>(id);
            }
            return articlesInCategory;
        }

        public void Create(Guid articleId, List<string> categoriesId)
        {
            var categoryInArtycle = new CategoryInArticle();
            categoryInArtycle.ArticleId = articleId;

            using (SqlConnection db = new SqlConnection(_connectionString))
            {
                foreach(var categoryId in categoriesId)
                {
                    categoryInArtycle.Id = Guid.NewGuid();
                    categoryInArtycle.CategoryId = new Guid(categoryId);
                    db.Insert(categoryInArtycle);
                }
            }
        }

        public void DeleteArticle(string id)
        {
            var query = "DELETE FROM CategoryInArticles WHERE ArticleId = @id";

            using (IDbConnection db = new SqlConnection(_connectionString))
            {  
                db.Execute(query, new { id });
            }
        }

        public void DeleteCategory(string id)
        {
            var query = "DELETE FROM CategoryInArticles WHERE CategoryId = @id";

            using (IDbConnection db = new SqlConnection(_connectionString))
            {
                db.Execute(query, new { id });
            }
        }

        public void Delete(string id)
        {
            using (IDbConnection db = new SqlConnection(_connectionString))
            {
                CategoryInArticle catInArt = new CategoryInArticle { Id = new Guid(id) };
                db.Delete(catInArt);
            }
        }
    }
}
