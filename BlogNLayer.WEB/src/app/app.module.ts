import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AppRoutingModule } from './app.routing.module';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { AppComponent } from './app.component';
import { CommentService } from './shared/service/comment.service';
import { ArticleService } from './shared/service/article.service';
import { CategoryService } from './shared/service/category.service';
import { ConstHelperService } from './shared/service/constHelper.service';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

@NgModule({
  declarations: [
    AppComponent
  ],

  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
    AppRoutingModule,
    HttpClientModule,
    BrowserAnimationsModule
  ],

  providers: [
    CommentService,
    ArticleService,
    CategoryService,
    ConstHelperService
  ],

  bootstrap: [AppComponent]
})
export class AppModule { }
