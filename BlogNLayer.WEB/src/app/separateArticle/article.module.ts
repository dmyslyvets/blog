import { FormsModule } from '@angular/forms';
import { NgModule } from '@angular/core';
import { ArticleComponent } from './article.component';
import { Routes, RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';

const routes: Routes = [
    { path: ":id", component: ArticleComponent }
  ];

@NgModule({
  declarations: [
    ArticleComponent
  ],
   
  imports: [ 
    RouterModule.forChild(routes),
    CommonModule,
    FormsModule
  ]
})

export class ArticleModule { }
