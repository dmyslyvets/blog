import { FormsModule } from '@angular/forms';
import { NgModule } from '@angular/core';
import { NewArticleComponent } from './newArticle.component';
import { DropDownsModule } from '@progress/kendo-angular-dropdowns';
import { Routes, RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';

const routes: Routes = [
  { path: "newArticle", component: NewArticleComponent }
];

@NgModule({
  declarations: [
    NewArticleComponent,
  ],

  imports: [
    RouterModule.forChild(routes),
    DropDownsModule,
    CommonModule,
    FormsModule
  ]
})

export class ArticleModule { }
