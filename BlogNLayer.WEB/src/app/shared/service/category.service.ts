import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Category } from '../models/Category';
import { ConstHelperService } from './constHelper.service';
import { Observable } from 'rxjs/Observable';

@Injectable()
export class CategoryService {

    constructor(private constHelper: ConstHelperService, private http: HttpClient) {
    }

    getAll() : Observable<Category[]> {
        return this.http.get((this.constHelper.getHost()) + 'api/Category/GetAll').map((data : Category[]) =>{return data;});
    }

    add(category: Category) {

        const body = { Title: category.title };
        return this.http.post(this.constHelper.getHost() + 'api/Category/Create', body, { responseType: "json" });
    }

    delete(id: string) {

        const body = id;
        this.http.post(this.constHelper.getHost() + 'api/Category/Delete/' + id, body).subscribe();
    }
} 