import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
 
@Injectable()
export class ConstHelperService {

    pth: string;
  
    constructor() {
        this.pth = "http://" + location.host + "/";
    }

    getHost(){
        return this.pth;
    }
} 