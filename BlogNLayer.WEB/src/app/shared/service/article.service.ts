import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { ArticleSend } from '../models/articleSend';
import { Category } from '../models/Category';
import { ConstHelperService } from './constHelper.service';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import { ArticleModel } from '../models/articleModel';

@Injectable()
export class ArticleService {

    constructor(private constHelper: ConstHelperService, private http: HttpClient) {

    }

    getAll() : Observable<ArticleModel[]> { 
        
        return  this.http.get(this.constHelper.getHost() + 'api/Article/GetAllArticle').map((data : ArticleModel[]) =>{return data});- 
    }

    get(id: string) : Observable<ArticleModel> {
        return this.http.get(this.constHelper.getHost() + 'api/Article/GetArticle/' + id).map((data : ArticleModel) =>{return data});;
    }

    getAllByCategory(id: string) : Observable<ArticleModel[]> {
        return this.http.get(this.constHelper.getHost() + 'api/Article/GetAllArticleByCategory/' + id).map((data : ArticleModel[]) =>{return data});;
    }

    delete(id: string) {
        const body = id;
        this.http.post(this.constHelper.getHost() + 'api/Article/Delete/' + id, body).subscribe();
    }

    post(article: ArticleSend) {
        const body = { Title: article.title, Author: article.author, Description: article.description, CategoryId: article.categoryId, };
        this.http.post(this.constHelper.getHost() + 'api/Article/CreateArticle', body).subscribe();
    }
} 
