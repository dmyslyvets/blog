import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Comment } from '../models/Comment';
import { ConstHelperService } from './constHelper.service';
import { CommentSend } from '../models/commentSent';

@Injectable()
export class CommentService {

    constructor(private constHelper: ConstHelperService, private http: HttpClient) {
    }

    addComment(comment: CommentSend) {

        this.http.post(this.constHelper.getHost() + 'api/Comment/CreateComment', comment).subscribe();
    }

    deleteComent(id: string) {

        const body = id;
        this.http.post(this.constHelper.getHost() + 'api/Comment/Delete/' + id, body).subscribe();
    }
} 