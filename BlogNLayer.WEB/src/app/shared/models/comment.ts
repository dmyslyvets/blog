export class Comment {

     public id: string;
     public nameAuthor : string;
     public commentary : string;
     public dateTime : string;
     public articleId : string;
}