import {NgModule} from "@angular/core";
import {Routes, RouterModule} from "@angular/router";
import {AppComponent} from "./app.component";
import { APP_BASE_HREF } from '@angular/common';
import { AllArticleComponent } from './allArticles/allArticle.component';
import { NewArticleComponent } from './newArticle/newArticle.component';
import { AllCategoryComponent } from './allCategory/allCategory.component';
import { EditCategoryComponent } from './editCategories/editCategory.component';
import { ArticleComponent } from './separateArticle/article.component';
 
const routes: Routes = [
  { path: "", loadChildren: './allArticles/allArticle.module#AllArticleModule' },
  { path: "allArticle", loadChildren: './allArticles/allArticle.module#AllArticleModule' },
  { path: "newArticle", loadChildren: './newArticle/newArticle.module#ArticleModule'},
  { path: "allCategory", loadChildren: './allCategory/allCategory.module#AllCategoryModule' },
  { path: "editCategory", loadChildren: './editCategories/editCategories.module#EditCategoryModule' },
  { path: 'article', loadChildren: './separateArticle/article.module#ArticleModule' } 
];

@NgModule({
    imports: [RouterModule.forRoot(routes)],
    exports: [RouterModule],
    providers: [{provide: APP_BASE_HREF, useValue : '/' }]
})
export class AppRoutingModule{}
